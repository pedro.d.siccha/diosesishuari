<?php

use App\Http\Controllers\ConductoresController;
use App\Http\Controllers\ConfiguracionesController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\EmpresasController;
use App\Http\Controllers\GuiaRemisionController;
use App\Http\Controllers\MaterialesController;
use App\Http\Controllers\TransporteController;
use App\Http\Controllers\VehiculosController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');

Route::middleware(['auth:sanctum', 'verified'])->get('/guiaremision', [GuiaRemisionController::class, 'index'])->name('listaGuiaRemision');
Route::middleware(['auth:sanctum', 'verified'])->get('/rguiaremision', [GuiaRemisionController::class, 'reporte'])->name('reporteGuiaRemision');

Route::middleware(['auth:sanctum', 'verified'])->get('/empresas', [EmpresasController::class, 'index'])->name('listaEmpresas');
Route::middleware(['auth:sanctum', 'verified'])->get('/rempresas', [EmpresasController::class, 'reporte'])->name('reporteEmpresas');

Route::middleware(['auth:sanctum', 'verified'])->get('/materiales', [MaterialesController::class, 'index'])->name('listaMateriales');
Route::middleware(['auth:sanctum', 'verified'])->get('/rmateriales', [MaterialesController::class, 'reporte'])->name('reporteMateriales');

Route::middleware(['auth:sanctum', 'verified'])->get('/transporte', [TransporteController::class, 'index'])->name('listaTransporte');
Route::middleware(['auth:sanctum', 'verified'])->get('/rtransporte', [TransporteController::class, 'reporte'])->name('reporteTransporte');

Route::middleware(['auth:sanctum', 'verified'])->get('/vehiculos', [VehiculosController::class, 'index'])->name('listaVehiculos');
Route::middleware(['auth:sanctum', 'verified'])->get('/rvehiculos', [VehiculosController::class, 'reporte'])->name('reporteVehiculos');

Route::middleware(['auth:sanctum', 'verified'])->get('/conductores', [ConductoresController::class, 'index'])->name('listaConductores');
Route::middleware(['auth:sanctum', 'verified'])->get('/rconductores', [ConductoresController::class, 'reporte'])->name('reporteConductores');

Route::middleware(['auth:sanctum', 'verified'])->get('/configuracion', [ConfiguracionesController::class, 'index'])->name('listaConfiguracion');
Route::middleware(['auth:sanctum', 'verified'])->get('/rconfiguracion', [ConfiguracionesController::class, 'reporte'])->name('reporteConfiguracion');
