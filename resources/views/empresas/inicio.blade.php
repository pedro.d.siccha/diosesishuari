<x-app-layout>
    @section('titulo')
        Lista de Empresas
    @endsection
    @section('contenido')
    <header class="page-header">
        <h2>Lista de Empresas</h2>
        <div class="right-wrapper pull-right">
            <ol class="breadcrumbs">
                <li>
                    <a href="{{ url('/dashboard') }}">
                        <i class="fa fa-home"></i>
                    </a>
                </li>
                <li><span>Empresas</span></li>
                <li><a href="{{ Route('listaEmpresas') }}">LISTA</a></li>
            </ol>
    
            <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
        </div>
    </header>

    <div class="col-md-12">
        <section class="panel">
            <header class="panel-heading">
                <div class="panel-actions">
                    <a href="#" class="fa fa-caret-down"></a>
                    <a href="#" class="fa fa-times"></a>
                </div>

                <h2 class="panel-title">Lista de Empresas</h2>
            </header>
            <div class="panel-body">
                <a class="mb-xs mt-xs mr-xs btn btn-xs btn-default" role="button" data-toggle="tooltip" data-placement="top" title="Nuevo Empresa" onclick="nuevo()"><i class="fa fa-plus"></i>  NUEVO</a>
                <div class="table-responsive">
                    <table class="table table-hover mb-none">
                        <thead>
                            <tr>
                                <th>RUC</th>
                                <th>Nombre</th>
                                <th>Dirección</th>
                                <th>Num. Contacto</th>
                                <th>Opciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>72690062</td>
                                <td>Mark</td>
                                <td>Otto Assd</td>
                                <td>10236546685</td>
                                <td>
                                    <div class="btn-group">
										<a class="mb-xs mt-xs mr-xs btn btn-xs btn-info" role="button" data-toggle="tooltip" data-placement="top" title="Ver Empresa" onclick="ver()"><i class="fa fa-info-circle"></i></a>
										<a class="mb-xs mt-xs mr-xs btn btn-xs btn-warning" role="button" data-toggle="tooltip" data-placement="top" title="Editar Empresa" onclick="editar()"><i class="fa fa-pencil"></i></a>
										<a class="mb-xs mt-xs mr-xs btn btn-xs btn-danger" role="button" data-toggle="tooltip" data-placement="top" title="Eliminar Empresa" onclick="eliminar()"><i class="fa fa-trash-o"></i></a>
									</div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </section>
    </div>

    <div class="modal inmodal fade" id="nuevo" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <form class="form-horizontal form-bordered" method="get">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                            class="sr-only">Close</span></button>
                    <h4 class="modal-title">Nueva Empresa</span></h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal form-bordered" method="get">
                        
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="inputDefault">Ruc</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" id="inputDefault" placeholder="Número de Ruc">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="inputDefault">Nombre</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" id="inputDefault" placeholder="Nombre de Empresa">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="inputDefault">Dirección</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" id="inputDefault" placeholder="Dirección">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="inputDefault">Referencia</label>
                            <div class="col-md-6">
                                <textarea class="form-control" rows="3" id="textareaDefault" placeholder="Referencia..."></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="inputDefault">Num. Contacto</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" id="inputDefault" placeholder="Dirección">
                            </div>
                        </div>
                    
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline btn-dark dim" data-dismiss="modal"> CANCELAR</button>
                    <button type="button" class="btn btn-outline btn-success dim" data-dismiss="modal"> GUARDAR</button>
                </div>
            </form>
            </div>
        </div>
    </div>

    <div class="modal inmodal fade" id="editar" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <form class="form-horizontal form-bordered" method="get">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                            class="sr-only">Close</span></button>
                    <h4 class="modal-title">Editar Empresa</span></h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal form-bordered" method="get">
                        
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="inputDefault">Ruc</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" id="inputDefault" placeholder="Número de Ruc">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="inputDefault">Nombre</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" id="inputDefault" placeholder="Nombre de Empresa">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="inputDefault">Dirección</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" id="inputDefault" placeholder="Dirección">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="inputDefault">Referencia</label>
                            <div class="col-md-6">
                                <textarea class="form-control" rows="3" id="textareaDefault" placeholder="Referencia..."></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="inputDefault">Num. Contacto</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" id="inputDefault" placeholder="Dirección">
                            </div>
                        </div>
                    
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline btn-dark dim" data-dismiss="modal"> CANCELAR</button>
                    <button type="button" class="btn btn-outline btn-success dim" data-dismiss="modal"> GUARDAR</button>
                </div>
            </form>
            </div>
        </div>
    </div>

    @endsection
    @section('script')
    <script>

        function nuevo(){
            $('#nuevo').modal('show');
        }

        function editar(){
            $('#editar').modal('show');
        }

        function ver(){
            $('#ver').modal('show');
        }

        function eliminar(){
            $('#eliminar').modal('show');
        }

    </script>
    @endsection
    </x-app-layout>