<x-app-layout>
@section('titulo')
    Lista de Materiales
@endsection
@section('contenido')
<header class="page-header">
    <h2>Lista de Materiales</h2>
    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="{{ url('/dashboard') }}">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li><span>Materiales</span></li>
            <li><a href="{{ Route('listaMateriales') }}">LISTA</a></li>
        </ol>

        <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
    </div>
</header>

<div class="col-md-12">
    <section class="panel">
        <header class="panel-heading">
            <div class="panel-actions">
                <a href="#" class="fa fa-caret-down"></a>
                <a href="#" class="fa fa-times"></a>
            </div>

            <h2 class="panel-title">Lista de Materiales</h2>
        </header>
        <div class="panel-body">
            <a class="mb-xs mt-xs mr-xs btn btn-xs btn-default" role="button" data-toggle="tooltip" data-placement="top" title="Nuevo Material" onclick="nuevo()"><i class="fa fa-plus"></i>  NUEVO</a>
            <div class="table-responsive">
                <table class="table table-hover mb-none">
                    <thead>
                        <tr>
                            <th>Cod.</th>
                            <th>Nombre</th>
                            <th>Unid. Medida</th>
                            <th>Peso/Medida</th>
                            <th>Costo Unid.</th>
                            <th>Opciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>72690062</td>
                            <td>Mark</td>
                            <td>Kg</td>
                            <td>5 Kg</td>
                            <td>S/. 1.50</td>
                            <td>
                                <div class="btn-group">
                                    <a class="mb-xs mt-xs mr-xs btn btn-xs btn-info" role="button" data-toggle="tooltip" data-placement="top" title="Ver Material" onclick="ver()"><i class="fa fa-info-circle"></i></a>
                                    <a class="mb-xs mt-xs mr-xs btn btn-xs btn-warning" role="button" data-toggle="tooltip" data-placement="top" title="Editar Material" onclick="editar()"><i class="fa fa-pencil"></i></a>
                                    <a class="mb-xs mt-xs mr-xs btn btn-xs btn-danger" role="button" data-toggle="tooltip" data-placement="top" title="Eliminar Material" onclick="eliminar()"><i class="fa fa-trash-o"></i></a>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </section>
</div>

<div class="modal inmodal fade" id="nuevo" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form class="form-horizontal form-bordered" method="get">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                        class="sr-only">Close</span></button>
                <h4 class="modal-title">Nuevo Material</span></h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal form-bordered" method="get">
                    
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="inputDefault">Código</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" id="inputDefault" placeholder="Código de material">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label" for="inputDefault">Material</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" id="inputDefault" placeholder="Nombre del material">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label" for="inputDefault">Unidad de Medida</label>
                        <div class="col-md-6">
                            <select data-plugin-selectTwo class="form-control populate">
                                <option value="">Seleccione Unidad de Medida...</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label" for="inputDefault">Peso</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" id="inputDefault" placeholder="Peso">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label" for="inputDefault">Precio</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" id="inputDefault" placeholder="Precio">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label" for="inputDefault">Detalles</label>
                        <div class="col-md-6">
                            <textarea class="form-control" rows="3" id="textareaDefault" placeholder="Detalles..."></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label">Foto Referencial</label>
                        <div class="col-md-6">
                            <div class="fileupload fileupload-new" data-provides="fileupload">
                                <div class="input-append">
                                    <div class="uneditable-input">
                                        <i class="fa fa-file fileupload-exists"></i>
                                        <span class="fileupload-preview"></span>
                                    </div>
                                    <span class="btn btn-default btn-file">
                                        <span class="fileupload-exists">Cambiar</span>
                                        <span class="fileupload-new">Subir</span>
                                        <input type="file" />
                                    </span>
                                    <a href="#" class="btn btn-default fileupload-exists" data-dismiss="fileupload">Eliminar</a>
                                </div>
                            </div>
                        </div>
                    </div>
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline btn-dark dim" data-dismiss="modal"> CANCELAR</button>
                <button type="button" class="btn btn-outline btn-success dim" data-dismiss="modal"> GUARDAR</button>
            </div>
        </form>
        </div>
    </div>
</div>

<div class="modal inmodal fade" id="editar" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form class="form-horizontal form-bordered" method="get">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                        class="sr-only">Close</span></button>
                <h4 class="modal-title">Editar Material</span></h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal form-bordered" method="get">
                    
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="inputDefault">Código</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" id="inputDefault" placeholder="Código de material">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label" for="inputDefault">Material</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" id="inputDefault" placeholder="Nombre del material">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label" for="inputDefault">Unidad de Medida</label>
                        <div class="col-md-6">
                            <select data-plugin-selectTwo class="form-control populate">
                                <option value="">Seleccione Unidad de Medida...</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label" for="inputDefault">Peso</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" id="inputDefault" placeholder="Peso">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label" for="inputDefault">Precio</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" id="inputDefault" placeholder="Precio">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label" for="inputDefault">Detalles</label>
                        <div class="col-md-6">
                            <textarea class="form-control" rows="3" id="textareaDefault" placeholder="Detalles..."></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label">Foto Referencial</label>
                        <div class="col-md-6">
                            <div class="fileupload fileupload-new" data-provides="fileupload">
                                <div class="input-append">
                                    <div class="uneditable-input">
                                        <i class="fa fa-file fileupload-exists"></i>
                                        <span class="fileupload-preview"></span>
                                    </div>
                                    <span class="btn btn-default btn-file">
                                        <span class="fileupload-exists">Cambiar</span>
                                        <span class="fileupload-new">Subir</span>
                                        <input type="file" />
                                    </span>
                                    <a href="#" class="btn btn-default fileupload-exists" data-dismiss="fileupload">Eliminar</a>
                                </div>
                            </div>
                        </div>
                    </div>
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline btn-dark dim" data-dismiss="modal"> CANCELAR</button>
                <button type="button" class="btn btn-outline btn-success dim" data-dismiss="modal"> ACTUALIZAR</button>
            </div>
        </form>
        </div>
    </div>
</div>

@endsection
@section('script')

<script src="{{ ('assetsHome/vendor/jquery-autosize/jquery.autosize.js') }}"></script>
<script src="{{ ('assetsHome/vendor/bootstrap-fileupload/bootstrap-fileupload.min.js') }}"></script>
<link rel="stylesheet" href="{{ ('assetsHome/vendor/bootstrap-fileupload/bootstrap-fileupload.min.css') }}" />

<script>

    function nuevo(){
        $('#nuevo').modal('show');
    }

    function ver(){
        $('#ver').modal('show');
    }

    function editar(){
        $('#editar').modal('show');
    }

    function eliminar(){
        $('#eliminar').modal('show');
    }

</script>
@endsection
</x-app-layout>