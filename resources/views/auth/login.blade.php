<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ ('assets/css/login.css') }}">
    <title>Ingresar</title>
</head>
<body>
    
        
        <div class='box'>
          <div class='box-form'>
            <div class='box-login-tab'></div>
            <div class='box-login-title'>
              <div class='i i-login'></div><h2>INGRESAR</h2>
            </div>
            <div class='box-login'>
                <form method="POST" action="{{ route('login') }}">
                    @csrf
              <div class='fieldset-body' id='login_form'>
                <button onclick="openLoginInfo();" class='b b-form i i-more' title='Más Información'></button>
                <p class='field'>
                  <label for='email'>CORREO</label>
                  <input type='text' id='email' name='email' title='CORREO' :value="old('email')" required autofocus/>
                  <span id='valida' class='i i-warning'></span>
                </p>
                <p class='field'>
                  <label for='password'>CONTRASEÑA</label>
                  <input type='password' id='password' name='password' title='Contraseña' required autocomplete="current-password"/>
                  <span id='valida' class='i i-close'></span>
                </p>
                
                <!--
                  <label class='checkbox'>
                    <input type='checkbox' value='TRUE' title='Keep me Signed in' /> Keep me Signed in
                  </label>
                -->
                    <input type='submit' id='do_login' value='INGRESAR' title='INGRESAR' />
              </div>
                </form>
            </div>
          </div>
        </div>
</body>
</html>
<script src="./assets/js/login.js"></script>