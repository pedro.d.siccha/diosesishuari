<x-app-layout>
    @section('titulo')
        Configuraciones
    @endsection
    @section('contenido')
    <header class="page-header">
        <h2>Configuraciones</h2>
        <div class="right-wrapper pull-right">
            <ol class="breadcrumbs">
                <li>
                    <a href="{{ url('/dashboard') }}">
                        <i class="fa fa-home"></i>
                    </a>
                </li>
                <li><span>Configuraciones</span></li>
            </ol>
    
            <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
        </div>
    </header>
    @endsection
    @section('script')
    @endsection
    </x-app-layout>