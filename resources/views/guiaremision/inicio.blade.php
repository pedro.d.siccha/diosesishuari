<x-app-layout>
    @section('titulo')
        Lista de Guias de Remisión
    @endsection
    @section('contenido')
    <header class="page-header">
        <h2>Lista de Guias de Remisión</h2>
        <div class="right-wrapper pull-right">
            <ol class="breadcrumbs">
                <li>
                    <a href="{{ url('/dashboard') }}">
                        <i class="fa fa-home"></i>
                    </a>
                </li>
                <li><span>Guias de Remisión</span></li>
                <li><a href="{{ Route('listaGuiaRemision') }}">LISTA</a></li>
            </ol>
    
            <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
        </div>
    </header>

    <div class="col-md-12">
        <section class="panel">
            <header class="panel-heading">
                <div class="panel-actions">
                    <a href="#" class="fa fa-caret-down"></a>
                    <a href="#" class="fa fa-times"></a>
                </div>

                <h2 class="panel-title">Lista de Guías de Remisión</h2>
            </header>
            <div class="panel-body">
                <a class="mb-xs mt-xs mr-xs btn btn-xs btn-default" role="button" data-toggle="tooltip" data-placement="top" title="Nueva Guía de Remisión" onclick="nuevo()"><i class="fa fa-plus"></i>  NUEVO</a>
                <div class="table-responsive">
                    <table class="table table-hover mb-none">
                        <thead>
                            <tr>
                                <th>Codigo</th>
                                <th>Origen</th>
                                <th>Destino</th>
                                <th>Fec. Salida</th>
                                <th>Estado</th>
                                <th>Opciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>001</td>
                                <td>Jangas</td>
                                <td>Lima</td>
                                <td>24/03/2021</td>
                                <td>PENDIENTE</td>
                                <td>
                                    <div class="btn-group">
										<a class="mb-xs mt-xs mr-xs btn btn-xs btn-info" role="button" data-toggle="tooltip" data-placement="top" title="Ver Guía de Remisión"><i class="fa fa-info-circle"></i></a>
										<a class="mb-xs mt-xs mr-xs btn btn-xs btn-warning" role="button" data-toggle="tooltip" data-placement="top" title="Editar Guía de Remisión"><i class="fa fa-pencil"></i></a>
										<a class="mb-xs mt-xs mr-xs btn btn-xs btn-danger" role="button" data-toggle="tooltip" data-placement="top" title="Eliminar Guía de Remisión"><i class="fa fa-trash-o"></i></a>
									</div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </section>
    </div>

    <div class="row">

        <div class="col-md-6">
            <section class="panel">
                <header class="panel-heading">
                    <div class="panel-actions">
                        <a href="#" class="fa fa-caret-down"></a>
                        <a href="#" class="fa fa-times"></a>
                    </div>
    
                    <h2 class="panel-title">Guías de Remisión Pendiente</h2>
                </header>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-hover mb-none">
                            <thead>
                                <tr>
                                    <th>Codigo</th>
                                    <th>Destino</th>
                                    <th>Fec. Salida</th>
                                    <th>Opciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>001</td>
                                    <td>Lima</td>
                                    <td>24/03/2021</td>
                                    <td>
                                        <div class="btn-group">
                                            <a class="mb-xs mt-xs mr-xs btn btn-xs btn-info" role="button" data-toggle="tooltip" data-placement="top" title="Ver Guía de Remisión"><i class="fa fa-info-circle"></i></a>
                                            <a class="mb-xs mt-xs mr-xs btn btn-xs btn-warning" role="button" data-toggle="tooltip" data-placement="top" title="Editar Guía de Remisión"><i class="fa fa-pencil"></i></a>
                                            <a class="mb-xs mt-xs mr-xs btn btn-xs btn-danger" role="button" data-toggle="tooltip" data-placement="top" title="Eliminar Guía de Remisión"><i class="fa fa-trash-o"></i></a>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </section>
        </div>
        
        <div class="col-md-6">
            <section class="panel">
                <header class="panel-heading">
                    <div class="panel-actions">
                        <a href="#" class="fa fa-caret-down"></a>
                        <a href="#" class="fa fa-times"></a>
                    </div>
    
                    <h2 class="panel-title">Guías de Remisión Finalizada</h2>
                </header>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-hover mb-none">
                            <thead>
                                <tr>
                                    <th>Codigo</th>
                                    <th>Destino</th>
                                    <th>Fec. Salida</th>
                                    <th>Opciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>001</td>
                                    <td>Lima</td>
                                    <td>24/03/2021</td>
                                    <td>
                                        <div class="btn-group">
                                            <a class="mb-xs mt-xs mr-xs btn btn-xs btn-info" role="button" data-toggle="tooltip" data-placement="top" title="Ver Guía de Remisión"><i class="fa fa-info-circle"></i></a>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </section>
        </div>

    </div>

    @endsection
    @section('script')
    <script>
        function nuevo(){
            $('#nuevo').modal('show');
        }
    </script>
    @endsection
    </x-app-layout>