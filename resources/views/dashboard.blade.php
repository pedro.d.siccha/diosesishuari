<x-app-layout>
@section('titulo')
    Inicio
@endsection
@section('contenido')
<header class="page-header">
    <h2>Inicio</h2>
    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="{{ url('/dashboard') }}">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li><span>Inicio</span></li>
        </ol>

        <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
    </div>
</header>

<section class="panel">
    <div class="panel-body">
        <div class="row">
            <div class="col-md-9">
                <div id="calendar"></div>
            </div>
            <div class="col-md-3">
                <p class="h4 text-light">Guias Pendientes</p>

                <hr />

                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-hover mb-none">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Salida</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>1</td>
                                    <td>29/03/2021</td>
                                    <td class="actions-hover actions-fade">
                                        <a href=""><i class="fa fa-pencil"></i></a>
                                        <a href="" class="delete-row"><i class="fa fa-trash-o"></i></a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>2</td>
                                    <td>30/03/2021</td>
                                    <td class="actions-hover actions-fade">
                                        <a href=""><i class="fa fa-pencil"></i></a>
                                        <a href="" class="delete-row"><i class="fa fa-trash-o"></i></a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>3</td>
                                    <td>31/03/2021</td>
                                    <td class="actions-hover actions-fade">
                                        <a href=""><i class="fa fa-pencil"></i></a>
                                        <a href="" class="delete-row"><i class="fa fa-trash-o"></i></a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('script')
<link rel="stylesheet" href="assetsHome/vendor/fullcalendar/fullcalendar.css" />
<link rel="stylesheet" href="assetsHome/vendor/fullcalendar/fullcalendar.print.css" media="print" />
<script src="assetsHome/vendor/fullcalendar/lib/moment.min.js"></script>
<script src="assetsHome/vendor/fullcalendar/fullcalendar.js"></script>
<script src="assetsHome/javascripts/pages/examples.calendar.js"></script>
@endsection
</x-app-layout>
