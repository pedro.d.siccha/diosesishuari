<x-app-layout>
    @section('titulo')
        Lista de Vehiculos
    @endsection
    @section('contenido')
    <header class="page-header">
        <h2>Lista de Vehiculos</h2>
        <div class="right-wrapper pull-right">
            <ol class="breadcrumbs">
                <li>
                    <a href="{{ url('/dashboard') }}">
                        <i class="fa fa-home"></i>
                    </a>
                </li>
                <li><span>Vehiculos</span></li>
                <li><a href="{{ Route('listaVehiculos') }}">LISTA</a></li>
            </ol>
    
            <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
        </div>
    </header>

    <div class="col-md-12">
        <section class="panel">
            <header class="panel-heading">
                <div class="panel-actions">
                    <a href="#" class="fa fa-caret-down"></a>
                    <a href="#" class="fa fa-times"></a>
                </div>

                <h2 class="panel-title">Lista de Vehiculos</h2>
            </header>
            <div class="panel-body">
                <a class="mb-xs mt-xs mr-xs btn btn-xs btn-default" role="button" data-toggle="tooltip" data-placement="top" title="Nuevo Vehiculo" onclick="nuevo()"><i class="fa fa-plus"></i>  NUEVO</a>
                <div class="table-responsive">
                    <table class="table table-hover mb-none">
                        <thead>
                            <tr>
                                <th>Placa</th>
                                <th>Marca</th>
                                <th>Modelo</th>
                                <th>Emp. Transporte</th>
                                <th>Opciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>72690062</td>
                                <td>Mark</td>
                                <td>Otto Assd</td>
                                <td>Emp 1</td>
                                <td>
                                    <div class="btn-group">
										<a class="mb-xs mt-xs mr-xs btn btn-xs btn-info" role="button" data-toggle="tooltip" data-placement="top" title="Ver Vehiculo" onclick="ver()"><i class="fa fa-info-circle"></i></a>
										<a class="mb-xs mt-xs mr-xs btn btn-xs btn-warning" role="button" data-toggle="tooltip" data-placement="top" title="Editar Vehiculo" onclick="editar()"><i class="fa fa-pencil"></i></a>
										<a class="mb-xs mt-xs mr-xs btn btn-xs btn-danger" role="button" data-toggle="tooltip" data-placement="top" title="Eliminar Vehiculo" onclick="eliminar()"><i class="fa fa-trash-o"></i></a>
									</div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </section>
    </div>

    <div class="modal inmodal fade" id="nuevo" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <form class="form-horizontal form-bordered" method="get">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                            class="sr-only">Close</span></button>
                    <h4 class="modal-title">Nuevo Vehiculo</span></h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal form-bordered" method="get">
                        
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="inputDefault">Placa</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" id="inputDefault" placeholder="Número de Placa">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="inputDefault">Marca</label>
                            <div class="col-md-6">
                                <select data-plugin-selectTwo class="form-control populate">
                                    <option value="">Seleccione Marca...</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="inputDefault">Modelo</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" id="inputDefault" placeholder="Modelo">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="inputDefault">Detalles</label>
                            <div class="col-md-6">
                                <textarea class="form-control" rows="3" id="textareaDefault" placeholder="Detalles..."></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="inputDefault">Empresa de Transportes</label>
                            <div class="col-md-6">
                                <select data-plugin-selectTwo class="form-control populate">
                                    <option value="">Seleccione Empresa de Transporte...</option>
                                </select>
                            </div>
                        </div>
    
                        <div class="form-group">
                            <label class="col-md-3 control-label">Foto de la Placa</label>
                            <div class="col-md-6">
                                <div class="fileupload fileupload-new" data-provides="fileupload">
                                    <div class="input-append">
                                        <div class="uneditable-input">
                                            <i class="fa fa-file fileupload-exists"></i>
                                            <span class="fileupload-preview"></span>
                                        </div>
                                        <span class="btn btn-default btn-file">
                                            <span class="fileupload-exists">Cambiar</span>
                                            <span class="fileupload-new">Subir</span>
                                            <input type="file" />
                                        </span>
                                        <a href="#" class="btn btn-default fileupload-exists" data-dismiss="fileupload">Eliminar</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label">Foto del Vehiculo</label>
                            <div class="col-md-6">
                                <div class="fileupload fileupload-new" data-provides="fileupload">
                                    <div class="input-append">
                                        <div class="uneditable-input">
                                            <i class="fa fa-file fileupload-exists"></i>
                                            <span class="fileupload-preview"></span>
                                        </div>
                                        <span class="btn btn-default btn-file">
                                            <span class="fileupload-exists">Cambiar</span>
                                            <span class="fileupload-new">Subir</span>
                                            <input type="file" />
                                        </span>
                                        <a href="#" class="btn btn-default fileupload-exists" data-dismiss="fileupload">Eliminar</a>
                                    </div>
                                </div>
                            </div>
                        </div>
    
                    
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline btn-dark dim" data-dismiss="modal"> CANCELAR</button>
                    <button type="button" class="btn btn-outline btn-success dim" data-dismiss="modal"> GUARDAR</button>
                </div>
            </form>
            </div>
        </div>
    </div>

    <div class="modal inmodal fade" id="editar" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <form class="form-horizontal form-bordered" method="get">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                            class="sr-only">Close</span></button>
                    <h4 class="modal-title">Editar Vehiculo</span></h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal form-bordered" method="get">
                        
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="inputDefault">Placa</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" id="inputDefault" placeholder="Número de Placa">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="inputDefault">Marca</label>
                            <div class="col-md-6">
                                <select data-plugin-selectTwo class="form-control populate">
                                    <option value="">Seleccione Marca...</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="inputDefault">Modelo</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" id="inputDefault" placeholder="Modelo">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="inputDefault">Detalles</label>
                            <div class="col-md-6">
                                <textarea class="form-control" rows="3" id="textareaDefault" placeholder="Detalles..."></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="inputDefault">Empresa de Transportes</label>
                            <div class="col-md-6">
                                <select data-plugin-selectTwo class="form-control populate">
                                    <option value="">Seleccione Empresa de Transporte...</option>
                                </select>
                            </div>
                        </div>
    
                        <div class="form-group">
                            <label class="col-md-3 control-label">Foto de la Placa</label>
                            <div class="col-md-6">
                                <div class="fileupload fileupload-new" data-provides="fileupload">
                                    <div class="input-append">
                                        <div class="uneditable-input">
                                            <i class="fa fa-file fileupload-exists"></i>
                                            <span class="fileupload-preview"></span>
                                        </div>
                                        <span class="btn btn-default btn-file">
                                            <span class="fileupload-exists">Cambiar</span>
                                            <span class="fileupload-new">Subir</span>
                                            <input type="file" />
                                        </span>
                                        <a href="#" class="btn btn-default fileupload-exists" data-dismiss="fileupload">Eliminar</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label">Foto del Vehiculo</label>
                            <div class="col-md-6">
                                <div class="fileupload fileupload-new" data-provides="fileupload">
                                    <div class="input-append">
                                        <div class="uneditable-input">
                                            <i class="fa fa-file fileupload-exists"></i>
                                            <span class="fileupload-preview"></span>
                                        </div>
                                        <span class="btn btn-default btn-file">
                                            <span class="fileupload-exists">Cambiar</span>
                                            <span class="fileupload-new">Subir</span>
                                            <input type="file" />
                                        </span>
                                        <a href="#" class="btn btn-default fileupload-exists" data-dismiss="fileupload">Eliminar</a>
                                    </div>
                                </div>
                            </div>
                        </div>
    
                    
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline btn-dark dim" data-dismiss="modal"> CANCELAR</button>
                    <button type="button" class="btn btn-outline btn-success dim" data-dismiss="modal"> GUARDAR</button>
                </div>
            </form>
            </div>
        </div>
    </div>

    @endsection

    @section('script')
    
    <script src="{{ ('assetsHome/vendor/jquery-autosize/jquery.autosize.js') }}"></script>
	<script src="{{ ('assetsHome/vendor/bootstrap-fileupload/bootstrap-fileupload.min.js') }}"></script>
    <link rel="stylesheet" href="{{ ('assetsHome/vendor/bootstrap-fileupload/bootstrap-fileupload.min.css') }}" />

    <script>

        function nuevo(){
            $('#nuevo').modal('show');
        }

        function ver(){
            $('#ver').modal('show');
        }

        function editar(){
            $('#editar').modal('show');
        }

        function eliminar(){
            $('#eliminar').modal('show');
        }

    </script>
    @endsection
    </x-app-layout>

    